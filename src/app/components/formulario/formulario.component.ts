import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { DatosI } from 'src/app/interfaces/datos';
import { ServiciosService } from '../servicios.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent implements OnInit {
  formulario!: FormGroup;

  valor1: string = '';
  number!: number;
  Valuenivel: number = 0;

  datos = new MatTreeNestedDataSource<DatosI>();
  control = new NestedTreeControl<DatosI>((node) => node.children);

  constructor(private fb: FormBuilder) {
    this.formulario = fb.group({
      niveles: [
        1,
        [Validators.min(1), Validators.max(100), Validators.required],
      ],
      repeticiones: [
        1,
        [Validators.min(1), Validators.max(100), Validators.required],
      ],
      valor1: ['', [Validators.required]],
      valor2: ['', [Validators.required]],
      valor3: ['', [Validators.required]],
      valor4: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {}


  getValue(num: number, values: string[]): string {
    if (num > 4) {
      return this.getValue(num - 4, values);
    } else {
      return values[num - 1];
    }
  }

  guardar(): void {
    console.log('guardar');
    console.log(this.formulario.value);
    this.Valuenivel = this.formulario.value.Nvel;
    this.valor1 = this.formulario.value.valor1;
  }
}
