import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DatosI } from '../interfaces/datos';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  constructor(public http: HttpClient) { }

  getInfo(): Observable<DatosI[]>{
    return this.http.get<DatosI[]>('./assets/data.json')
  }

}
